import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import Async from '../../helpers/asyncComponent';
import moment from 'moment';
import { uploadCSV, editObject, getCurrentUser, getObjectWithId, adminLogger, getRoles, getAllObjects } from "../../helpers/parseHelper";
import qs from 'query-string'
import { ActionBtn } from './style';
import {
    Input,
    InputNumber,
    Select,
    Radio,
    Form,
    DatePicker,
    Icon,
    message,
    Tabs,
    Button,
    Space,
    TreeSelect,
} from 'antd';
import { MinusCircleOutlined, PlusOutlined, DownOutlined } from '@ant-design/icons';
import config from "./config.js";
import { LoadingOutlined, CloudUploadOutlined } from '@ant-design/icons';
import visualizeFormat from "./visualizeFormat";
import Upload, { getUploadFileValue } from '../../components/uploadFile'


const className = config.className
const { Option } = Select;

class EditComponent extends Component {
    formRef = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            data: null,
            parseObject: null,
            objectId: null,
            adminLevel: 1,
            roles: [],
            types: [],
            typeData: [],
            parentTypeInfo: [],
            stationData: null,
        };

    }
    componentDidMount = async () => {
        const user = await getCurrentUser()
        const adminLevel = user.get('adminLevel')
        const userData = user.toJSON()
        const data = { adminID: userData.objectId }
        this
            .formRef
            .current
            .setFieldsValue(data)
        this.loadData(this.props)
        const roles = await getRoles()
        this.setState({ roles })

        const parse = await qs.parse(window.location.search)
        const { objectId } = parse
        const stationData = await getObjectWithId('Station', objectId);
        const json = stationData.toJSON();
        console.log('station Data:', json);
        this.setState({ stationData: json });

        //qurery data types
        const types = await getAllObjects('DataType')
        this.setState({ typeData: types, types: types })

        //map data  Types to TreeSelect Format
        let rawTypeData = types.reverse().map(data => {
            return {
                name: data.name,
                title: data.name,
                value: data.objectId,
                parentId: data.ParentId,
                objectId: data.objectId,
                disabled:true
            }
        });

        //console.log(rawTypeData);

        let rootType = rawTypeData.filter(data => data.parentId === undefined);
        rawTypeData.map(item => {
            const CurrentItem = rawTypeData.filter(data => data.parentId === item.objectId).map(item => {
                item.disabled = false
                return item
            })
            item.children = CurrentItem
            //console.log('CurrentItem:', CurrentItem);
        })
        //console.log('New Data:', rootType);
        this.setState({ types: rootType });

    }
    UNSAFE_componentWillReceiveProps = async (nextProps) => {
        this.loadData(nextProps)
    }
    loadData = async (props) => {
        const { objectId } = props
        const data = await getObjectWithId(className, objectId);
        const json = data.toJSON()
        //console.log('json', json);
        json.fileDate = moment(json.fileDate.iso);

        this.setState({ parentTypeInfo: json.typeData });
        this.setState({ parentAreaInfo: json.areaData });

        //Clear MetaData In FormRef
        const MetaData = { MetaData: [] };
        this
            .formRef
            .current
            .setFieldsValue(MetaData)


        this.setState({ data }, () => {
            this
                .formRef
                .current
                .setFieldsValue(json)
        })
    }

    handleSubmit = async () => {
        //console.log('Form Values', values)
        const promise = this.formRef.current.validateFields();
        this.setState({ loading: true });
        const { data, areaData, country, typeData, parentAreaInfo, parentTypeInfo, stationData } = this.state
        const values = await Promise.resolve(promise)
        //console.log('values:', values);
        //getCurrentUser and set current user value to formRef
        const user = await getCurrentUser()

        if (values) {
            values.objectId = data.id;
            values.admin = user; //store current user to admin field pointer
            values.typeName = typeData.find(item => item.objectId === values.typeId).name || undefined
            values.typeData = parentTypeInfo

            if (values.fileDate) {
                values.fileDate = values.fileDate.toDate();
            }

            if (values.areaId && Array.isArray(values.areaId)) {
                values.areaId.push(stationData.areaId);
            } else {
                values.areaId = [];
                values.areaId.push(stationData.areaId);
            }

            if (values.areaName && Array.isArray(values.areaName)) {
                values.areaName.push(stationData.areaName);
            } else {
                values.areaName = [];
                values.areaName.push(stationData.areaName);
            }

            if (values.countryId && Array.isArray(values.countryId)) {
                values.countryId.push(stationData.countryId);
            } else {
                values.countryId = [];
                values.countryId.push(stationData.countryId);
            }

            if (values.countryName && Array.isArray(values.countryName)) {
                values.countryName.push(stationData.countryName);
            } else {
                values.countryName = [];
                values.countryName.push(stationData.countryName);
            }

            if (!values.fileData[0].url) {
                if (values.fileData) {
                    const file = await uploadCSV(values.fileData[0]);
                    console.log('Upload New file', values.fileData[0]);
                    values.fileData[0].url = file.url;
                    values.fileName = values.fileData[0]['name'];
                    values.fileUrl = file.url;
                }
            } else {
                console.log('not upload file Edit Only!');
            }

            await data.save(values, { useMasterKey: true });
            notification('success', 'Save Done');
            await adminLogger(className, 'EDIT', data.toJSON())
            this.props.onCreateSuccess();
        }

        this.setState({ loading: false });
    };

    handleCountryIdChange = (value) => {
        const { country } = this.state
        country.map(item => {

            if (item.objectId === value) {
                console.log(item.name)
                this.setState({ areaName: item.name })

                const data = { countryName: item.name };

                this
                    .formRef
                    .current
                    .setFieldsValue(data)
            }
        }
        )
    };

    handleParentIDChange = (value) => {
        const { country, area } = this.state
        if (value === '-') {
            const data = { parentname: '-' };
            this
                .formRef
                .current
                .setFieldsValue(data)
        }

        area.map(item => {
            if (item.objectId === value) {
                console.log(item.name)
                this.setState({ areaName: item.name })
                const data = { parentname: item.name };
                this
                    .formRef
                    .current
                    .setFieldsValue(data)
            }
        }
        )
    };



    //Function Get All Parents Area
    getAllParents(parentID) {
        const { areaData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = areaData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                const Info = JSON.stringify(parentInfo);
                //console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.parentID;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

    //Function Get All Parents Area
    getAllParentsType(parentID) {
        const { typeData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = typeData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.ParentId;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

    onAreaSelect = (selectedKeys, info) => {
        //console.log('selectedKeys', info.objectId);
        const parentID = info.parentID;
        const parentAreaInfo = this.getAllParents(parentID);
        this.setState({ parentAreaInfo });
        const data = { areaName: info.name, areaId: info.objectId };
        this
            .formRef
            .current
            .setFieldsValue(data)
    };

    onTypeSelect = (selectedKeys, info) => {
        console.log('selectedKeys', info);
        const parentId = info.parentId;
        const parentTypeInfo = this.getAllParentsType(parentId);
        this.setState({ parentTypeInfo });
        const data = { typeName: info.name, typeId: info.objectId };
        this
            .formRef
            .current
            .setFieldsValue(data)
    };

    extraFileView = ({ value }) => {
        console.log('extraFileView', value);
        return value ? value.map(item => item.name).join(', ') : ''
    }

    render() {
        const { country, area, areaName, types, roles } = this.state
        return (
            <Form 
            {...formItemLayout} 
            ref={this.formRef} 
            onFinishFailed={(errorInfo) => {
                //console.log('Failed:', errorInfo);
                this.setState({ loading: false });
            }}
            >
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="typeId"
                    label="Type"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <TreeSelect
                        style={{
                            width: '100%',
                        }}
                        dropdownStyle={{
                            maxHeight: 400,
                            overflow: 'auto',
                        }}
                        switcherIcon={<DownOutlined />}
                        treeData={types}
                        showSearch={false}
                        showLine
                        placeholder="Please select"
                        treeDefaultExpandAll
                        onSelect={this.onTypeSelect}
                    />
                </Form.Item>
                <Form.Item
                    name="fileDate"
                    label="Date"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <DatePicker />
                </Form.Item>
                <Form.Item
                    name="fileData"
                    label="CSV File"
                    valuePropName="fileList"
                    getValueFromEvent={getUploadFileValue}
                >
                    <Upload />
                </Form.Item>

                <Form.Item
                    name="visualizeFormat"
                    label="Visulize Format"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <TreeSelect
                        style={{
                            width: '100%',
                        }}
                        dropdownStyle={{
                            maxHeight: 400,
                            overflow: 'auto',
                        }}
                        switcherIcon={<DownOutlined />}
                        treeData={visualizeFormat}
                        showSearch={false}
                        showLine
                        placeholder="Please select"
                        treeDefaultExpandAll
                    />
                </Form.Item>

                <Form.Item
                    name="adminID"
                    label="Admin ID"
                    style={{ display: 'none' }}
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.List
                    name="metaData"
                    label="MetaData"
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}
                >
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(({ key, name, ...restField }) => (
                                <Space
                                    key={key}
                                    style={{
                                        display: 'flex',
                                        marginBottom: 8,
                                        marginLeft: 50,
                                    }}
                                    align="baseline"
                                >
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'Key']}
                                        label="Key"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Missing Key name',
                                            },
                                        ]}
                                    >
                                        <Input placeholder="Key" />
                                    </Form.Item>
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'Value']}
                                        label="Value"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Missing Value',
                                            },
                                        ]}
                                    >
                                        <Input placeholder="Value" />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(name)} />
                                </Space>
                            ))}
                            <Form.Item
                                style={
                                    {
                                        marginLeft: 80,
                                    }
                                }
                            >
                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                    Add MetaData field
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>

                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        loading={this.state.loading}
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Update
                    </ActionBtn>
                </Form.Item>
            </Form>

        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(EditComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};