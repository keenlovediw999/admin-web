import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import Async from '../../helpers/asyncComponent';
import moment from 'moment';
import { editObject, getCurrentUser, getObjectWithId, adminLogger, getRoles } from "../../helpers/parseHelper";
import qs from 'query-string'
import { ActionBtn } from './style';
import {
    Input,
    Select,
    Radio,
    Form,
    DatePicker,
    Upload,
    Icon,
    message,
    Tabs
} from 'antd';
import config from "./config.js";
import { LoadingOutlined, CloudUploadOutlined } from '@ant-design/icons';
const className = config.className
const { Option } = Select;


class EditComponent extends Component {
    formRef = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            data: null,
            parseObject: null,
            objectId: null,
            adminLevel: 1,
            roles: []
        };

    }
    componentDidMount = async () => {
        this.loadData(this.props)
        const roles = await getRoles()
        this.setState({ roles })
    }
    UNSAFE_componentWillReceiveProps = async (nextProps) => {
        this.loadData(nextProps)
    }
    loadData = async (props) => {
        const { objectId } = props
        const data = await getObjectWithId(className, objectId);
        const json = data.toJSON()
        console.log('json', json);
        this.setState({ data }, () => {
            this
                .formRef
                .current
                .setFieldsValue(json)
        })
    }

    handleSubmit = async () => {
        const { data } = this.state
        const promise = this.formRef.current.validateFields()
        const values = await Promise.resolve(promise)
        if (values) {
            await data.save(values, { useMasterKey: true });
            notification('success', 'Save Done');
            await adminLogger(className, 'EDIT', data.toJSON())
            this.props.onCreateSuccess();
        }
    };


    render() {
        const { roles } = this.state
        return (
            <Form {...formItemLayout} ref={this.formRef} >
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="index"
                    label="Index"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Submit
                    </ActionBtn>
                </Form.Item>
            </Form>

        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(EditComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};