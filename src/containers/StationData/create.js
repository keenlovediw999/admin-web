import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import { getObjectWithId, uploadCSV, uploadParseFile, createObject, adminLogger, getCurrentUser, getRoles, getAllObjects } from "../../helpers/parseHelper";
import { ActionBtn } from './style';
import {
    Input,
    InputNumber,
    Select,
    Form,
    Button,
    Space,
    TreeSelect,
    DatePicker,
} from 'antd';
import { MinusCircleOutlined, PlusOutlined, DownOutlined } from '@ant-design/icons';
import config from "./config.js";
import visualizeFormat from "./visualizeFormat";
import Upload, { getUploadFileValue } from '../../components/uploadFile'
import qs from 'query-string'

const className = config.className
const { Option } = Select;
const { TreeNode } = TreeSelect;


class CreateComponent extends Component {
    formRef = React.createRef();
    countryNameRef = React.createRef();
    //valueRef = React.useRef();

    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            adminLevel: 1,
            roles: [],
            types: [],
            typeData: [],
            parentTypeInfo: [],
            stationData: null,
        };
    }
    componentDidMount = async () => {
        const user = await getCurrentUser()
        const adminLevel = user.get('adminLevel')
        const userData = user.toJSON()
        const data = { adminID: userData.objectId }
        this
            .formRef
            .current
            .setFieldsValue(data)
        this.setState({ adminLevel })
        const roles = await getRoles()
        this.setState({ roles })

        const parse = await qs.parse(window.location.search)
        const { objectId } = parse
        const stationData = await getObjectWithId('Station', objectId);
        const json = stationData.toJSON();
        //console.log('station Data:', json);
        this.setState({ stationData: json });


        //qurery data types
        const types = await getAllObjects('DataType')
        this.setState({ typeData: types, types: types })

        //map data  Types to TreeSelect Format
        let rawTypeData = types.reverse().map(data => {
            return {
                name: data.name,
                title: data.name,
                value: data.objectId,
                parentId: data.ParentId,
                objectId: data.objectId,
                disabled:true
            }
        });

        //console.log(rawTypeData);

        let rootType = rawTypeData.filter(data => data.parentId === undefined);
        rawTypeData.map(item => {
            const CurrentItem = rawTypeData.filter(data => data.parentId === item.objectId).map(item => {
                item.disabled = false
                return item
            })
            item.children = CurrentItem
            //console.log('CurrentItem:', CurrentItem);
        })
        console.log('New Data:', rootType);
        this.setState({ types: rootType });

    }

    handleSubmit = async e => {
        //console.log('Form Values', values)
        const promise = this.formRef.current.validateFields();
        this.setState({ loading: true });
        const { typeData, parentTypeInfo, stationData } = this.state
        const values = await Promise.resolve(promise)
        //console.log('Form Values', values)
        //getCurrentUser and set current user value to formRef
        const user = await getCurrentUser()
        const userData = user.toJSON()
        const Admin = { admin: userData.objectId }
        this
            .formRef
            .current
            .setFieldsValue(Admin)

        values.areaId = [];
        values.areaName = [];
        values.countryId = [];
        values.countryName = [];

        if (values) {
            values.typeName = typeData.find(item => item.objectId === values.typeId).name || undefined
            values.typeData = parentTypeInfo
            values.stationId = stationData.objectId;
            values.areaId.push(stationData.areaId);
            values.areaName.push(stationData.areaName);
            values.countryId.push(stationData.countryId);
            values.countryName.push(stationData.countryName);

            if (values.fileDate) {
                values.fileDate = values.fileDate.toDate();
            }

            if (values.fileData) {
                const file = await uploadCSV(values.fileData[0]);
                //console.log('file', values.fileData[0]);
                values.fileData[0].url = file.url;
                values.fileName = values.fileData[0]['name'];
                values.fileUrl = file.url;
            }

            const res = await createObject(className, values)
            notification(res.type, res.msg)
            if (res.type == 'success') {
                await adminLogger(className, 'CREATE', res.object.toJSON())
                this.formRef.current.resetFields();
                this.props.onCreateSuccess();
            }
        }

        this.setState({ loading: false });

    };


    //Function Get All Parents Area
    getAllParents(parentID) {
        const { areaData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = areaData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                const Info = JSON.stringify(parentInfo);
                //console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.parentID;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

    //Function Get All Parents Area
    getAllParentsType(parentID) {
        const { typeData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = typeData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.ParentId;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

    onTypeSelect = (selectedKeys, info) => {
        console.log('selectedKeys', info);
        const parentId = info.parentId;
        const parentTypeInfo = this.getAllParentsType(parentId);

        this.setState({ parentTypeInfo });
        const data = { typeName: info.name, typeId: info.objectId };
        this
            .formRef
            .current
            .setFieldsValue(data)
    };


    render() {
        const { types, roles } = this.state
        const value = null;
        return (
            <Form
                {...formItemLayout}
                ref={this.formRef}
                onFinishFailed={(errorInfo) => {
                    //console.log('Failed:', errorInfo);
                    this.setState({ loading: false });
                }}
            >
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="typeId"
                    label="Type"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <TreeSelect
                        style={{
                            width: '100%',
                        }}
                        dropdownStyle={{
                            maxHeight: 400,
                            overflow: 'auto',
                        }}
                        switcherIcon={<DownOutlined />}
                        treeData={types}
                        showSearch={false}
                        showLine
                        placeholder="Please select"
                        treeDefaultExpandAll
                        onSelect={this.onTypeSelect}
                    />
                </Form.Item>
                <Form.Item
                    name="fileDate"
                    label="Date"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <DatePicker />
                </Form.Item>
                <Form.Item name="fileData" label="CSV File" valuePropName="fileList" getValueFromEvent={getUploadFileValue}>
                    <Upload />
                </Form.Item>
                <Form.Item
                    name="visualizeFormat"
                    label="Visulize Format"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <TreeSelect
                        style={{
                            width: '100%',
                        }}
                        dropdownStyle={{
                            maxHeight: 400,
                            overflow: 'auto',
                        }}
                        switcherIcon={<DownOutlined />}
                        treeData={visualizeFormat}
                        showSearch={false}
                        showLine
                        placeholder="Please select"
                        treeDefaultExpandAll
                    />
                </Form.Item>
                <Form.Item
                    name="adminID"
                    label="Admin ID"
                    style={{ display: 'none' }}
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.List
                    name="metaData"
                    label="MetaData"
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}
                >
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(({ key, name, ...restField }) => (
                                <Space
                                    key={key}
                                    style={{
                                        display: 'flex',
                                        marginBottom: 8,
                                        marginLeft: 50,
                                    }}
                                    align="baseline"
                                >
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'Key']}
                                        label="Key"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Missing Key name',
                                            },
                                        ]}
                                    >
                                        <Input placeholder="Key" />
                                    </Form.Item>
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'Value']}
                                        label="Value"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Missing Value',
                                            },
                                        ]}
                                    >
                                        <Input placeholder="Value" />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(name)} />
                                </Space>
                            ))}
                            <Form.Item
                                style={
                                    {
                                        marginLeft: 80,
                                    }
                                }
                            >
                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                    Add MetaData field
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>


                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        loading={this.state.loading}
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Save
                    </ActionBtn>
                </Form.Item>
            </Form>
        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(CreateComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};