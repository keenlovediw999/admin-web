import moment from 'moment';
import React from 'react';
import { Tag } from 'antd'
import ReactTooltip from 'react-tooltip';

const optionsCursorTrueWithMargin = {
    followCursor: false,
    shiftX: 0,
    shiftY: 0
};
const config = {
    className: "LayerData",
    defaultQueryKey: "fileName",
    listTitle: "Layer Data",
    filterOptions: [
        {
            value: "fileName",
            key: "fileName",
            label: "File Name"
        },
    ],
    columns: [
        {
            title: 'File Date',
            dataIndex: 'fileDate',
            key: 'fileDate',
            render: (data, row) => (data ? <div>{moment(data.iso).format('DD/MM/YYYY')}</div> : null)
        }, {
            title: 'File Name',
            dataIndex: 'fileName',
            key: 'fileName',
            sorter: (a, b) => sorter(a, b, 'fileName'),
            render: (text, record) => {
                return (
                    <div>
                        <a href={record.fileUrl} target="_blank" data-tip data-for={text}>{text}</a>
                        <ReactTooltip id={text}>
                            <img src={record.fileUrl} style={{ width: 300 }} />
                        </ReactTooltip>
                    </div>
                )
            }
        },
        {
            title: 'Legend File',
            dataIndex: 'legendFileName',
            key: 'legendFileName',
            sorter: (a, b) => sorter(a, b, 'legendFileName'),
            render: (text, record) => {
                return (
                    <div>
                        <a href={record.legendUrl} target="_blank" data-tip data-for={text}>{text}</a>
                        <ReactTooltip id={text}>
                            <img src={record.legendUrl} style={{ width: 300 }} />
                        </ReactTooltip>
                    </div>
                )
            }
        }
    ]
}

const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}


export default config