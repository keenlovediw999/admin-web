import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import { createObject, adminLogger, getCurrentUser, getRoles, getAllObjects } from "../../helpers/parseHelper";
import { ActionBtn } from './style';
import {
    Input,
    Select,
    Form,
    TreeSelect,
} from 'antd';
import config from "./config.js";


const className = config.className
const { Option } = Select;

class CreateComponent extends Component {
    formRef = React.createRef();
    countryNameRef = React.createRef();
    //valueRef = React.useRef();

    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            adminLevel: 1,
            roles: [],
            country: [],
            area: [],
            areaName: '',
            areaData: [],
        };
    }
    componentDidMount = async () => {
        const user = await getCurrentUser()
        const adminLevel = user.get('adminLevel')
        this.setState({ adminLevel })
        const roles = await getRoles()
        this.setState({ roles })
        const country = await getAllObjects('Country')
        this.setState({ country })
        const area = await getAllObjects('Area')
        this.setState({ area })
    }

    handleSubmit = async e => {
        const { area } = this.state;
        const promise = this.formRef.current.validateFields()
        const values = await Promise.resolve(promise)
        console.log('values',values)
        if (values) {

            if (values.parentID == null || values.parentID == undefined) {
                values.parentID = undefined
                values.parentname = undefined
            } else {
                values.parentname = area.find(item => item.objectId === values.parentId).name || undefined
            }
   
            const res = await createObject(className, values)
            notification(res.type, res.msg)
            if (res.type == 'success') {
                await adminLogger(className, 'CREATE', res.object.toJSON())
                this.formRef.current.resetFields();
                this.props.onCreateSuccess();
            }

        }
    };

    handleCountryIdChange = (value) => {

        const { country, areaName } = this.state
        console.log('Selected:', value)

        country.map(item => {

            if (item.objectId === value) {
                console.log(item.name)
                this.setState({ areaName: item.name })

                const data = { countryName: item.name };

                this
                    .formRef
                    .current
                    .setFieldsValue(data)
            }
        }
        )
    };

    handleParentIDChange = (value) => {
        const { country, area, areaName } = this.state
        console.log('Selected:', value)

        if (value==='-'){
            const data = { parentname: '-' };
            this
                .formRef
                .current
                .setFieldsValue(data)
        }

        area.map(item => {
            if (item.objectId === value) {
                console.log(item.name)
                this.setState({ areaName: item.name })
                const data = { parentname: item.name };
                this
                    .formRef
                    .current
                    .setFieldsValue(data)
            } 
        }
        )
    };

    render() {
        const { country, area, areaName, roles } = this.state

        return (
            <Form
                {...formItemLayout}
                ref={this.formRef}
            >
                <Form.Item
                    name="parentID"
                    label="Parent Area">
                    <Select>
                        <Option value={undefined} key={undefined}>{'No Parent'}</Option>
                        {area.map(item => (
                            <Option value={item.objectId} key={item.objectId}>{item.name}</Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="countryId"
                    label="Country"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Select
                        onChange={(value) => this.handleCountryIdChange(value)}
                    >
                        {country.map(item => (
                            <Option value={item.objectId} key={item.objectId}>{item.name}</Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="countryName"
                    label="country Name"
                    style={{ display: 'none' }}
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}
                >
                    <Input ref={this.countryNameRef}/>
                </Form.Item>
                <Form.Item
                    name="description"
                    label="Description"
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Save
                    </ActionBtn>
                </Form.Item>
            </Form>
        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(CreateComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};