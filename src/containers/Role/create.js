import React, { Component } from 'react';
import { connect } from "react-redux";
import { notification } from "../../components";
import { createObject, adminLogger, getCurrentUser, countCustomerByAdminId } from "../../helpers/parseHelper";
import { ActionBtn } from './style';
import {
    Input,
    Select,
    Radio,
    Form,
    InputNumber,
    Upload,
    Row, Col
} from 'antd';
import config from "./config.js";
import globalConfig from "../../config";
import axios from 'axios'
const className = config.className
const { Option } = Select;
const API_URL = globalConfig.parseServerUrl.replace('/parse', '/api/v1');


class CreateComponent extends Component {
    formRef = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            customerId: '',
            admin: null
        };
    }
    handleSubmit = async e => {
        this.setState({ loading: true })
        const promise = this.formRef.current.validateFields()
        const values = await Promise.resolve(promise)
        if (values) {
            const data = { name:values.name }
            delete values.name
            const list = Object.values(values).filter(item => item !== undefined)
            let permissionItems = []
            list.forEach(item => {
                permissionItems = [...permissionItems,...item]
            })
            data.permissionItems = permissionItems
            const res = await createObject(className, data)
            if (res.type == 'success') {
                notification(res.type, res.msg)
                await adminLogger('Roles', 'CREATE', res.object.toJSON())
                this.formRef.current.resetFields();
                this.props.onCreateSuccess();
            } else {
                notification(res.type, res.msg)
            }
            this.setState({ loading: false })
        }
    };
    render() {
        return (
            <Form
                {...formItemLayout}
                ref={this.formRef}>
                <Form.Item
                    name="name"
                    label="Role Name"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>        
                <Form.Item name="ADMIN" label="Admin" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('ADMIN') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                <Form.Item name="ROLE" label="Permission" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('ROLE') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                <Form.Item name="ACTIVITY" label="Activity" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('ACTIVITY') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                <Form.Item name="SETTING" label="Setting" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('SETTING') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                <Form.Item name="STATION" label="Station Data" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('STATION') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                <Form.Item name="OVERLAY" label="Overlay Data" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('OVERLAY') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                <Form.Item name="AREA" label="Area Data" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('AREA') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                <Form.Item name="COUNTRY" label="Country Data" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('COUNTRY') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                <Form.Item name="LAYERS" label="Layers Data" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('LAYERS') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                <Form.Item name="SITE" label="Site Data" >
                    <Select mode="multiple" placeholder="Please select" >
                        {
                            config.permissionsList.filter(item => item.indexOf('SITE') === 0).map(item => (<Option key={item}>{item}</Option>))
                        }
                    </Select>
                </Form.Item>
                
                <Form.Item {...tailFormItemLayout}>
                    <center>
                        <ActionBtn
                            type="primary"
                            loading={this.state.loading}
                            htmlType="submit"
                            onClick={this.handleSubmit}
                            style={{
                                width: 120
                            }}>
                            Submit
                        </ActionBtn>
                    </center>
                </Form.Item>
            </Form>

        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(CreateComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 9
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 24,
            offset: 0
        }
    }
};