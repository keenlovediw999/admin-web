import React, { Component } from 'react';
import LayoutContentWrapper from '../components/utility/layoutWrapper';
import ContentHolder from '../components/utility/contentHolder';
import Box from "../components/utility/box";
import { Row, Col, Statistic } from 'antd';
import { countAll } from '../helpers/parseHelper'

export default class extends Component {

  state = {
    stations:0,
    stationsData:0,
    layers:0,
    layerData:0
  }
  componentDidMount = async () => {
    const stations = await countAll({ className: 'Station' })
    this.setState({stations})
    const stationsData = await countAll({ className: 'StationData' })
    this.setState({stationsData})
    const layers = await countAll({ className: 'LayerList' })
    this.setState({layers})
    const layerData = await countAll({ className: 'LayerData' })
    this.setState({layerData})
  }


  render() {
    const {stations,stationsData,layerData,layers} = this.state
    return (
      <LayoutContentWrapper style={{ height: '100vh' }}>
        <ContentHolder style={{
          width: '100%',
          marginTop: 0
        }}>
          <Row gutter={8} style={{ marginBottom: 8 }}>
            <Col style={{ marginBottom: 8 }} lg={12} md={12} sm={24} xs={24}>
              <Box>
                <h1 style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>{'Stations Count'}</h1>
                <Statistic value={stations || 0} valueStyle={{ textAlign: 'center', fontSize: 32, fontWeight: 'bold', color: '#f7A271' }} />
              </Box>
            </Col>
            <Col style={{ marginBottom: 8 }} lg={12} md={12} sm={24} xs={24} >
              <Box>
                <h1 style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>{'Sub Stations Count'}</h1>
                <Statistic value={stationsData || 0} valueStyle={{ textAlign: 'center', fontSize: 32, fontWeight: 'bold', color: '#f7A271' }} />
              </Box>
            </Col>
            <Col style={{ marginBottom: 8 }} lg={12} md={24} sm={24} xs={24} >
              <Box>
                <h1 style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>{'Layers Count'}</h1>
                <Statistic value={layers || 0} valueStyle={{ textAlign: 'center', fontSize: 32, fontWeight: 'bold', color: '#6B7AA1' }} />
              </Box>
            </Col>

            <Col style={{ marginBottom: 8 }} lg={12} md={12} sm={24} xs={24} >
              <Box>
                <h1 style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>{'Sub Layers Count'}</h1>
                <Statistic value={layerData || 0} valueStyle={{ textAlign: 'center', fontSize: 32, fontWeight: 'bold', color: '#3797A4' }} />
              </Box>
            </Col>
          </Row>
        </ContentHolder>
      </LayoutContentWrapper>
    );
  }
}
