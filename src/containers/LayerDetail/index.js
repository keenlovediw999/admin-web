import React, { Component } from "react";
import Box from "../../components/utility/box";
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import { Col, Row, Checkbox } from 'antd';
import { getObjectWithId } from "../../helpers/parseHelper";
import LayerData from "../LayerData";
import qs from 'query-string'
import {
  ActionBtn,
  Label,
  TitleWrapper,
  ActionWrapper,
  ComponentTitle,
  TableWrapper,
  ButtonHolders
} from './style';


export default class BlogComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      layerData: null,
    };
}

  componentDidMount = async () => {
    const parse = await qs.parse(window.location.search)
    this.loadData(parse)
  }

  loadData = async (parse) => {
    const { objectId } = parse
    const layerData = await getObjectWithId('LayerList',objectId);
    const json  = layerData.toJSON();
    this.setState({ layerData: json });
  }

  render() {
    const { layerData } = this.state;
    console.log('LayerData', layerData);
    return (
      <LayoutWrapper>
        <TitleWrapper>
         {layerData && (<ComponentTitle>{`Layer : ${layerData.name}`}</ComponentTitle>)} 
        </TitleWrapper>
        <Box>
          <Row gutter={[3, 3]}>
            <Col span={24}>
              <LayerData objectId />
            </Col>
          </Row>
        </Box>
      </LayoutWrapper>
    );
  }
}
