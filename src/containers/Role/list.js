import React, { Component } from 'react';
import { getAllObjects, addCustomerBalance, adminLogger, changePassword, destroyObjectWithId, getCurrentUser } from "../../helpers/parseHelper";
import ContentHolder from '../../components/utility/contentHolder';
import Popconfirms from '../../components/feedback/popconfirm';
import {
    ActionBtn,
    Label,
    TitleWrapper,
    ActionWrapper,
    ComponentTitle,
    TableWrapper,
    ButtonHolders
} from './style';
import { Input, Select, Pagination, Button, Modal, Row, Col, Table, Form, DatePicker } from 'antd';
import { withRouter } from "react-router-dom";
import { notification } from "../../components";
import config from "./config.js";
import globalConfig from "../../config";
import axios from 'axios'
import CreateComponent from "./create"
import EditComponent from "./edit"
import moment from 'moment';
import {
    DollarOutlined,
    ShakeOutlined,
    CloseOutlined
} from '@ant-design/icons';
const { Search } = Input;
const { Option } = Select;

const className = config.className
const columns = config.columns
const API_URL = globalConfig.parseServerUrl.replace('/parse', '/api/v1');


class ListTableComponent extends Component {
    formRef = React.createRef();

    constructor(props) {
        super(props);
        this.state = {
            dataOnTable: [],
            isLoading: true,
            dataLimit: 99999,
            dataSkip: 0,
            dataCount: 0,
            isQuery: false,
            searchValue: "",
            selectedRowKeys: [],
            currentPage: 1,
            queryKey: config.defaultQueryKey,
            sendMsgModalVisible: false,
            selectedUser: null,
            isShowEditModal: false,
            isShowCreateModal: false,
            selectedId: null,
            admin: null,
            customerCount: 0,
            addBalanceAmount: 0,
            topupBalanceAmount: 0,
            currentPassword: '',
            currentUser: null, pageSize: 20,
        };
        this.sendMsgInput = React.createRef();

    }

    componentDidMount = async () => {
        this.simpleLoadData();
    }
    componentWillMount = async () => {
        const currentUser = await getCurrentUser();
        this.setState({ currentUser })
    }
    simpleLoadData = async () => {
        const dataOnTable = await getAllObjects(className);
        this.setState({ dataOnTable, isLoading: false });
    }

    handleRecord = async (actionName, obj) => {
        if (actionName == 'edit') {
            this.setState({ selectedId: obj.objectId }, () => {
                //console.log('selectedId',this.state.selectedId);
                this.setState({ isShowEditModal: true })
            });
        } else if (actionName == 'delete') {
            const res = await destroyObjectWithId(className, obj.objectId)
            notification(res.type, res.msg)
            await adminLogger('Bank', 'DESTROY', obj)
            this.simpleLoadData()
        } else if (actionName == 'connectApi') {
            this.openScbApiSetting(obj);
        }
    };
    handleFilterByChange = (value) => {
        //console.log(`handleFilterByChange : ${value}`)
        this.setState({ queryKey: value })
    }
    handleSearchChange = (event) => {
        this.setState({ searchValue: event.target.value })
    }

    renderCreateModal = () => {
        return (
            <Modal title={`Create ${className}`} visible={this.state.isShowCreateModal} style={{ top: 20 }} footer={null} onCancel={() => { this.setState({ isShowCreateModal: false }) }}>
                <CreateComponent onCreateSuccess={this.onCreateSuccess} />
            </Modal>
        )
    }
    renderEditModal = () => {
        const { selectedId, isShowEditModal } = this.state
        return (
            <Modal title={`Edit ${className}`} visible={isShowEditModal} style={{ top: 20 }} footer={null} onCancel={() => { this.setState({ isShowEditModal: false }) }}>
                <EditComponent onCreateSuccess={this.onCreateSuccess} objectId={selectedId} />
            </Modal>
        )
    }

    onCreateSuccess = () => {
        this.setState({ isShowCreateModal: false });
        this.setState({ isShowEditModal: false });
        this.simpleLoadData();
    }

    render() {
        const { dataOnTable, queryKey, searchValue, currentUser } = this.state;
        let filtered = searchValue === "" ? dataOnTable : dataOnTable.filter(item => item[queryKey].indexOf(searchValue) >= 0)
        columns[4] = {
            title: 'Actions',
            key: 'action',
            fixed: 'right',
            width: 100,
            render: (text, row) => {
                return (
                    <ActionWrapper>
                        <a
                            onClick={this
                                .handleRecord
                                .bind(this, 'edit', row)}
                            href="javascript:void(0)">
                            {'Edit'}
                        </a>
                        {
                            row.bankCode === '014' ? (<a
                                style={{ color: 'orange' }}
                                onClick={this
                                    .handleRecord
                                    .bind(this, 'connectApi', row)}
                                href="javascript:void(0)">
                                {'ตั้งค่า SCB APP'}
                            </a>) : null
                        }
                        <Popconfirms
                            title="Are you sure to delete this record?"
                            okText="Yes"
                            cancelText="No"
                            placement="topRight"
                            onConfirm={this
                                .handleRecord
                                .bind(this, 'delete', row)}>
                            <a className="deleteBtn" href="">
                                <i className="ion-android-delete" />
                            </a>
                        </Popconfirms>
                    </ActionWrapper>
                );
            }
        }

        return (
            <ContentHolder>
                <TitleWrapper>
                    <ComponentTitle>{config.listTitle}</ComponentTitle>
                    {this.renderCreateModal()}
                    {this.renderEditModal()}
                    <ButtonHolders>
                        <Label
                            style={{
                                marginRight: 15
                            }}>Filter by</Label>
                        <Select
                            defaultValue={config.defaultQueryKey}
                            style={{
                                width: 180,
                                marginRight: 15
                            }}
                            onChange={this
                                .handleFilterByChange
                                .bind(this)}>
                            {config
                                .filterOptions
                                .map(obj => {
                                    return (
                                        <Option value={obj.value}>{obj.label}</Option>
                                    )
                                })}
                        </Select>
                        <Search
                            allowClear
                            placeholder="input search text"
                            value={this.state.searchValue}
                            onChange={this
                                .handleSearchChange
                                .bind(this)}
                            onSearch={value => { }}
                            style={{
                                width: 200,
                                marginRight: 20,
                                marginTop: 3
                            }} />

                        <ActionBtn
                            type="default"
                            onClick={() => {
                                this.setState({
                                    isLoading: true,
                                    dataSkip: 0,
                                    currentPage: 1
                                }, () => {
                                    this.simpleLoadData()
                                })
                            }}>
                            Refresh
                        </ActionBtn>
                        <ActionBtn
                            type="primary"
                            onClick={() => {
                                this.setState({ isShowCreateModal: true });
                            }}>
                            Create
                        </ActionBtn>
                    </ButtonHolders>
                </TitleWrapper>
                {
                    currentUser ? <Table
                        rowKey="objectId"
                        bordered
                        columns={columns}
                        loading={this.state.isLoading}
                        dataSource={filtered}
                        onChange={(pagination) => {
                            console.log('pagination', pagination);
                            const { pageSize } = pagination
                            this.setState({ pageSize })
                        }}
                        pagination={{
                            pageSize: this.state.pageSize,
                            hideOnSinglePage: true
                        }} /> : null
                }

            </ContentHolder>
        )

    }
}

export default withRouter(ListTableComponent)

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 8
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 24,
            offset: 0
        }
    }
};