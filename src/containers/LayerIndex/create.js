import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import { createObject, adminLogger, getCurrentUser, getRoles } from "../../helpers/parseHelper";
import { ActionBtn } from './style';
import {
    Input,
    Select,
    Form,
} from 'antd';
import config from "./config.js";


const className = config.className
const { Option } = Select;

class CreateComponent extends Component {
    formRef = React.createRef();
    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            adminLevel: 1,
            roles: []
        };
    }
    componentDidMount = async () => {
        const user = await getCurrentUser()
        const adminLevel = user.get('adminLevel')
        this.setState({ adminLevel })
        const roles = await getRoles()
        this.setState({ roles })
    }

    handleSubmit = async e => {
        const promise = this.formRef.current.validateFields()
        const values = await Promise.resolve(promise)
        //console.log('values',values)
        if (values) {
            const res = await createObject(className, values)
            notification(res.type, res.msg)
            if (res.type == 'success') {
                await adminLogger(className, 'CREATE', res.object.toJSON())
                this.formRef.current.resetFields();
                this.props.onCreateSuccess();
            }

        }
    };

    render() {
        const { adminLevel, roles } = this.state
        return (
            <Form
                {...formItemLayout}
                ref={this.formRef}>
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="index"
                    label="Index"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Submit
                    </ActionBtn>
                </Form.Item>
            </Form>
        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(CreateComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};