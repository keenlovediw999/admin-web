import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import asyncComponent from '../../helpers/AsyncFunc';

const routes = [
  {
    path: '',
    component: asyncComponent(() => import('../dashboard')),
  },
  {
    path: 'admin',
    component: asyncComponent(() => import('../AdminUser')),
  },
  {
    path: 'role',
    component: asyncComponent(() => import('../Role')),
  },
  {
    path: 'country',
    component: asyncComponent(() => import('../Country')),
  },
  {
    path: 'area',
    component: asyncComponent(() => import('../Area')),
  },
  {
    path: 'type',
    component: asyncComponent(() => import('../Type')),
  },
  {
    path: 'station',
    component: asyncComponent(() => import('../Station')),
  },
  {
    path: 'stationdetail',
    component: asyncComponent(() => import('../StationDetail')),
  },
  {
    path: 'layers',
    component: asyncComponent(() => import('../Layers')),
  },{
    path: 'layerdata',
    component: asyncComponent(() => import('../LayerData')),
  },{
    path: 'layerdetail',
    component: asyncComponent(() => import('../LayerDetail')),
  },{
    path: 'layerIndex',
    component: asyncComponent(() => import('../LayerIndex')),
  },{
    path: 'siteChannel',
    component: asyncComponent(() => import('../SiteChannel')),
  },
];

class AppRouter extends Component {
  render() {
    const { url, style } = this.props;
    return (
      <div style={style}>
        {routes.map(singleRoute => {
          const { path, exact, ...otherProps } = singleRoute;
          return (
            <Route
              exact={exact === false ? false : true}
              key={singleRoute.path}
              path={`${url}/${singleRoute.path}`}
              {...otherProps}
            />
          );
        })}
      </div>
    );
  }
}

export default AppRouter;
