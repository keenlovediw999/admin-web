import React, { Component } from "react";
import Box from "../../components/utility/box";
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import List from "./list";

export default class BlogComponent extends Component {
  render() {
    return (
      <LayoutWrapper>
        <Box>
            <List />
        </Box>
      </LayoutWrapper>
    );
  }
}
