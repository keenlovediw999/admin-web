import moment from 'moment';
import React from 'react';
import { DownOutlined, IdcardTwoTone } from '@ant-design/icons';
import { Badge, Dropdown, Menu, Space, Table, Tag, Typography  } from 'antd';

const { Title } = Typography;

const config = {
    className: "DataType",
    defaultQueryKey: "name",
    listTitle: "Type List",
    expandedRowRender: true,
    filterOptions: [
        {
            value: "name",
            key: "name",
            label: "Type"
        }, {
            value: "description",
            key: "description",
            label: "Description"
        },
    ],
    columns: [
        {
            title: 'Type',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => sorter(a, b, 'name'),
           
        }, {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
            sorter: (a, b) => sorter(a, b, 'description'),
        }
    ]
}

const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}


export default config;