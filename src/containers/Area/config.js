import moment from 'moment';
import React from 'react';
import {Tag} from 'antd'


// Collection : Area
// 	- name          is store name of Area
// 	- parentID      is store parentID of root Area
// 	- parentname    is store parent name of root Area
// 	- description   is for Area more description 

const config = {
    className: "Area",
    defaultQueryKey: "name",
    listTitle: "Area List",
    filterOptions: [
        {
            value: "name",
            key: "name",
            label: "Area Name"
        },{
            value: "countryName",
            key: "countryName",
            label: "Country Name"
        },
    ],
    columns: [
        {
            title: 'Area Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => sorter(a, b, 'name'),
        }, {
            title: 'Country Name',
            dataIndex: 'countryName',
            key: 'countryName',
            sorter: (a, b) => sorter(a, b, 'countryName'),
        }, {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
            sorter: (a, b) => sorter(a, b, 'description'),
        }
    ]
}

const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}


export default config